function launchForum() {
	popForum = window.open("forum_login.jsp",'forum','width=648,height=560,toolbar=yes,location=yes,directories=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes');
}

function launchParents() {
	popParents = window.open("parents.jsp",'parents','width=550,height=475,toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no');
}

function launchFAQ() {
	popFAQ = window.open("faq.jsp",'faq','width=565,height=586,toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no');
}

function launchLegend() {
		popLegend = window.open("legend_popup.jsp",'behindLegend','width=565,height=586,toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no');
}

function launchLegendHTML() {
	if (isFlash6) { 
		popLegend = window.open("legend_popup.jsp",'behindLegend','width=565,height=586,toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no');
		return false;
	} else {
		return true;
	}
}

function launchVideo(x, quality) {
	var path = "";
	if (quality=="high") {
		path += "video.jsp?movie=" + x;
		playerWindow = window.open(path,'video_big','width=580,height=520,toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=0,resizable=0');
	} else {
		path += "video_low.jsp?movie=" + x;
		playerWindow = window.open(path,'video_small','width=340,height=340,toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=0,resizable=0');
	}
	playerWindow.focus();
}
	

function launchNewsletter() {
	popNewsletter = window.open("newsletter.jsp",'newsletter','width=634,height=550,toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes');
}

function launchTingle() {
	popNewsletter = window.open("tingle.jsp",'tingle','width=634,height=550,toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes');
}

function launchTingleVideo() {
	popTingleVid = window.open("video_tingle.jsp",'tingleVid','width=740,height=432,toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no');
	popTingleVid.focus();
}

function launchSailing() {
	window.location="resume.jsp";
}

function launchSailingPopup() {
	var popSailing = window.open("sailing/sailing.jsp",'sailing','width=730,height=450,toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no');
	popSailing.focus();
}

function Showlayer(which,how) {
	if (isNav || isExp) {
		eval ('document.'+pre+which+suf+'.visibility="'+how+'"');
		return;
	}
	else if (isW3C || isOpera) {
		obj=document.getElementById(which);
		obj.style.visibility=how;
	}
}


// functions for fading things out in IE and mozilla

function mozFadeOut() {
	var oDiv = document.getElementById("blender");
	var opacity = oDiv.style.MozOpacity;
	if (opacity <= 0) {
		Showlayer("splashLogos", "hidden");
		return;
	} else {
		oDiv.style.MozOpacity = (opacity - 0.025);
		window.setTimeout("mozFadeOut()", 25);
	}
}

function fadeOut() {
	if (isExp && Vmajor >= 4) {
		oDiv = blender;
		oDiv.style.filter="blendTrans(duration=2)";
		// Make sure the filter is not playing.
		if (oDiv.filters.blendTrans.status != 2) {
			oDiv.filters.blendTrans.apply();
			oDiv.style.visibility="hidden";
			oDiv.filters.blendTrans.play();
		}
	} else if (isMoz) {
		oDiv = document.getElementById("blender");
		//alert(oDiv.style.MozOpacity);
		//oDiv.style.MozOpacity = 0.975;
		window.setTimeout("mozFadeOut()", 25);
	} else if (!isNav) {
		Showlayer("splashLogos", "hidden");
	}
}

function launchWp(num, wpsize) {
	wpwin = window.open("http://media.zelda.com/zelda/gcn/img/dl/wallpaper" + num + "_" + wpsize + ".jpg","wallpaper");
	wpwin.focus();
}

function launchFigurinePDF() {
		popFigurine = window.open("http://media.zelda.com/zelda/gcn/downloads/figurines.pdf",'figurines','width=600,height=600,toolbar=yes,location=yes,directories=no,status=yes,menubar=yes,scrollbars=yes,resizable=yes');
}
