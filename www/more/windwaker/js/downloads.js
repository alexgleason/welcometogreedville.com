function launchInstructions(file) {
	popForum = window.open(file,'instructions','width=339,height=314,toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no');
}

function swapImage(strElement, objImage) {	
// this function swaps the image identified by strElement for a new one preloaded as objImage
	if (document.images && objImage && objImage.src && objImage.src != "") {
		if (document.images[strElement]) {
			document.images[strElement].src = objImage.src;
		}
	}
}

function swapImageNoPre(strElement, newImage) {	
// this function swaps the image identified by strElement for a new one, identified by a string
		if (document.images) {
		if (document.images[strElement]) {
			document.images[strElement].src = newImage;
		}
	}
}