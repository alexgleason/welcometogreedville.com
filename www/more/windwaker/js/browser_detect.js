<!--
//=================================
// General Browser Detection Script
// Revision: 2001.01.03
// Seb Chevrel for Second Story
//=================================

isNav=false;
isW3C=false;
isExp=false;
isOpera=false;
isNOT=false;
isMac=false;
isMoz=false;

// Detect browser and define pre/suf-fixes
browser=navigator.appName;
version=navigator.appVersion;
Vmajor=parseInt(navigator.appVersion);
Vminor=parseFloat(navigator.appVersion);

if (browser=="Netscape") {
	if (Vmajor==4)
	{
		isNav=true; pre='layers.'; suf='';
		window.captureEvents(Event.RESIZE);	// handle the resize bug on NN
	}
	else if (Vmajor==5)	{
		isW3C=true;
		isMoz=true;
	}
	else isNOT=true;
}
else if (browser=="Microsoft Internet Explorer") {
	
	if ( version.indexOf('MSIE 5.0; Macintosh;') != -1 )  {
		isExp=true;
		pre='all.';
		suf='.style';
	}
	// IE 4 to 5.5 return 4 as the version
	else if ( (Vmajor==4) ) {
		isExp=true;
		pre='all.';
		suf='.style';
	}
	else isNOT=true;
}
else if (browser=="Opera") {
	if (Vmajor==4) isOpera=true;
	else isNOT=true;
}

if (version.indexOf('Mac') != -1) isMac=true;

//-->
